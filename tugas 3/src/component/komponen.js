import React, {Component} from 'react';
import {View, Text, StyleSheet, ScrollView, Image} from 'react-native';

class Flexbox extends Component {
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.one}>
                    <Image source={require('/Users/Diana/nurul/src/assets/insta.jpg')} style={{width:150, height:60}}/>
                    <Image source={require('/Users/Diana/nurul/src/assets/love.jpg')} style={{width:40,height:40, marginLeft:120}}/>
                    <Image source={require('/Users/Diana/nurul/src/assets/kirim.jpg')} style={{width:40, height:40}}/>
                    <Text style={styles.teks}>10</Text>
                </View>
                <View style={styles.two}>
                    <ScrollView>
                        <ScrollView horizontal> 
                            <Image source={require('/Users/Diana/nurul/src/assets/foto.jpg')} style={styles.foto}/>
                            <Image source={require('/Users/Diana/nurul/src/assets/foto.jpg')} style={styles.foto}/>
                            <Image source={require('/Users/Diana/nurul/src/assets/foto.jpg')} style={styles.foto}/>
                            <Image source={require('/Users/Diana/nurul/src/assets/foto.jpg')} style={styles.foto}/>
                            <Image source={require('/Users/Diana/nurul/src/assets/foto.jpg')} style={styles.foto}/>
                            <Image source={require('/Users/Diana/nurul/src/assets/foto.jpg')} style={styles.foto}/>
                            <Image source={require('/Users/Diana/nurul/src/assets/foto.jpg')} style={styles.foto}/>
                        </ScrollView>
                        
                        <Image source={require('/Users/Diana/nurul/src/assets/isi.jpg')} style={styles.isi}/>
                        <Image source={require('/Users/Diana/nurul/src/assets/isi.jpg')} style={styles.isi}/>
                        <Image source={require('/Users/Diana/nurul/src/assets/isi.jpg')} style={styles.isi}/>
                        <Image source={require('/Users/Diana/nurul/src/assets/isi.jpg')} style={styles.isi}/>
                        <Image source={require('/Users/Diana/nurul/src/assets/isi.jpg')} style={styles.isi}/>
                        <Image source={require('/Users/Diana/nurul/src/assets/isi.jpg')} style={styles.isi}/>
                        <Image source={require('/Users/Diana/nurul/src/assets/isi.jpg')} style={styles.isi}/>
                        <Image source={require('/Users/Diana/nurul/src/assets/isi.jpg')} style={styles.isi}/>
                        <Image source={require('/Users/Diana/nurul/src/assets/isi.jpg')} style={styles.isi}/>
                        <Image source={require('/Users/Diana/nurul/src/assets/isi.jpg')} style={styles.isi}/>
                    </ScrollView>
                </View>
                <View style={styles.three}>
                  <Image source={require('/Users/Diana/nurul/src/assets/home.jpg')} style={styles.bawah}/>
                  <Image source={require('/Users/Diana/nurul/src/assets/cari.jpg')} style={styles.bawah}/>
                  <Image source={require('/Users/Diana/nurul/src/assets/plus.jpg')} style={styles.bawah}/>
                  <Image source={require('/Users/Diana/nurul/src/assets/shop.jpg')} style={styles.bawah}/>
                  <Image source={require('/Users/Diana/nurul/src/assets/profil.jpg')} style={styles.bawah}/>
                </View>
            </View>
        )
    }
}

const styles= StyleSheet.create({
    container: {
        flex:1,
        backgroundColor:'white'
    },
    one: {
        flex:1,
        flexDirection:'row',
        alignContent:'center',
        alignItems:'center'
    },
    two: {
        flex:9,
        backgroundColor: 'white'
    },
    three: {
        flex:1,
        backgroundColor: 'white',
        flexDirection:'row',
        justifyContent:'space-around'
    },
    foto: {
        width:70,
        height:70,
        borderRadius:35,
        borderColor:'red',
        marginLeft:8,
        borderWidth:2
    },
    isi: {
        width:360,
        height:600
    },
    bawah: {
        width: 40,
        height: 40
    },
    teks :{
        fontSize:15,
        color:'white',
        backgroundColor:'red',
        width:20,
        height:20,
        borderRadius:12,
        position:'absolute',
        top:6,
        right:7
    }
});

export default Flexbox;